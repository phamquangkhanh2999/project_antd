import React, { useState } from 'react';
import {
  DownOutlined,
  ArrowLeftOutlined,
  ArrowRightOutlined,
} from '@ant-design/icons';
import {
  Button,
  Modal,
  Form,
  Input,
  Row,
  Checkbox,
  Col,
  Dropdown,
  Menu,
  Space,
  Typography,
  Select,
} from 'antd';

const menu = (
  <Menu
    selectable
    defaultSelectedKeys={['3']}
    items={[
      {
        key: '1',
        label: 'Item 1',
      },
      {
        key: '2',
        label: 'Item 2',
      },
      {
        key: '3',
        label: 'Item 3',
      },
    ]}
  />
);

function RegisterPages() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [formLayout, setFormLayout] = useState('horizontal');
  const [form] = Form.useForm();
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const onFinish = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <React.Fragment>
      <Button type='primary' onClick={showModal}>
        Register
      </Button>
      <Modal
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={false}
        width={1200}
      >
        <div className='register-wrapper'>
          <div className='register-left'>
            <div className='image'>
              <img
                src='https://imgs.search.brave.com/dpi_T-T39DsnrM1HK2yRSDiRP99Wd8D9JUYtM1pcfp0/rs:fit:900:600:1/g:ce/aHR0cDovL2ZpdHNt/YWxsYnVzaW5lc3Mu/Y29tL3dwLWNvbnRl/bnQvdXBsb2Fkcy8y/MDE3LzA2LzE1LU9u/bGluZS1TaG9wcGlu/Zy1TdGF0aXN0aWNz/LUZ0cmQzMi5qcGc'
                alt=''
              />
            </div>
            <div className='register-left-wrapper'>
              <h3 className='title-register'>
                Quản lý dễ dành <br /> Bán hàng đơn giản
              </h3>
              <p className='text-register'>Hỗ trợ đăng ký 12345678</p>
            </div>
          </div>
          <div className='register-right'>
            <div className='form-wrapper'>
              <div className='form-title'>
                <h3 className='form-title-item'>
                  Hair Salon, Nalia, Massage & Spa
                </h3>
                <p>Chọn nghành hàng khác</p>
              </div>
              <Form
                name='basic'
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete='off'
                layout='vertical'
              >
                <Row gutter={[4, 4]}>
                  <Col xxl={24} xl={24} sm={24} xs={24}>
                    <Form.Item label='' name='username'>
                      <Input placeholder='Họ tên' className='form-input' />
                    </Form.Item>
                  </Col>
                  <Col xxl={24} xl={24} sm={24} xs={24}>
                    <Row>
                      <Col xxl={2} xl={2} sm={2} xs={2}>
                        <Dropdown overlay={menu}>
                          <Typography.Link>
                            <Space>
                              VN
                              <DownOutlined />
                            </Space>
                          </Typography.Link>
                        </Dropdown>
                      </Col>
                      <Col xxl={10} xl={10} sm={10} xs={10}>
                        <Form.Item
                          label=''
                          name=''
                          style={{ marginRight: '10px' }}
                        >
                          <Input
                            placeholder='Điện thoại'
                            className='form-input'
                          />
                        </Form.Item>
                      </Col>
                      <Col xxl={12} xl={12} sm={12} xs={12}>
                        <Input placeholder='Email' className='form-input' />
                      </Col>
                    </Row>
                  </Col>
                  <Col xxl={24} xl={24} sm={24} xs={24}>
                    <Form.Item label='' name='username'>
                      <Select placeholder='Tỉnh/Thành phố - Quận/Huyện'>
                        <Select.Option key={'city'}>city</Select.Option>
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col xxl={24} xl={24} sm={24} xs={24}>
                    <Form.Item label='' name='username'>
                      <Input
                        placeholder='Đặt tên cho gian hàng của bạn'
                        suffix='.test.com'
                        className='form-input'
                      />
                    </Form.Item>
                  </Col>

                  <Col xxl={24} xl={24} sm={24} xs={24}>
                    <Row>
                      <Col xxl={12} xl={12} sm={12} xs={12}>
                        <Form.Item
                          label=''
                          name='username'
                          style={{ marginRight: '10px' }}
                        >
                          <Input
                            placeholder='Tên Đăng nhập'
                            className='form-input'
                          />
                        </Form.Item>
                      </Col>
                      <Col xxl={12} xl={12} sm={12} xs={12}>
                        <Form.Item label='' name='username'>
                          <Input
                            placeholder='Mật Khẩu'
                            className='form-input'
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                  </Col>
                  <Col xxl={24} xl={24} sm={24} xs={24}>
                    <Row>
                      <Col xxl={12} xl={12} sm={12} xs={12}>
                        <Button className='btn-sms'>
                          Nhận mã xác thực qua SMS
                        </Button>
                      </Col>
                      <Col xxl={12} xl={12} sm={12} xs={12}>
                        <Form.Item label='' name='username'>
                          <Input
                            placeholder='Nhập mã xác thực'
                            className='form-input'
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Form.Item>
                  <Space className='space'>
                    <Button htmlType='submit' className='btn-back'>
                      <ArrowLeftOutlined /> Quay lại
                    </Button>
                    <Button htmlType='submit' className='btn-submit'>
                      Đăng Ký
                      <ArrowRightOutlined />
                    </Button>
                  </Space>
                </Form.Item>
              </Form>
            </div>
          </div>
        </div>
      </Modal>
    </React.Fragment>
  );
}

export default RegisterPages;
