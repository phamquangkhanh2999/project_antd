import './App.css';
import RegisterPages from './pages/Register/RegisterPages';


function App() {
  return (
    <div className="App">
      <RegisterPages />
    </div>
  );
}

export default App;
